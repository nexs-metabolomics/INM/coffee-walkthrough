---
output: 
  html_document: 
    df_print: kable
    highlight: espresso
    theme: lumen
    css: styles.css
editor_options: 
  chunk_output_type: console
title: "Installing software"
---

## Getting R ready

To follow along on your own PC during this presentation you need to (on windows):

* Install [R](https://cran.r-project.org/bin/windows/base/) 
* Install [RStudio](https://www.rstudio.com/products/rstudio/download/) or the [Preview version](https://rstudio.com/products/rstudio/download/preview/) that gives you the latest features.
* For Windows install [Rtools](https://cran.r-project.org/bin/windows/Rtools/)
* At the University of Copenhagen unfortunately the computers are setup in a way that makes installing packages properly difficult. To solve this do:
  * Open the windows file browser
  * right-click "This PC" --> "Properties"
  * On the left click "Advanced system settings"
  * Click "Environmental Variables..."
  * Click "New"
  * Type "R_LIBS_USER" in the first box (no brackets) and "C:\\Users\\XXXYYY\\Documents\\R\\win-library" in the second box (again no brackets). You should replace XXXYYY with your KU username.
* Restart R and RStudio if you had started them before making the above change.
* In RStudio in the upper right corner select "Open project..." and browse to and select the coffee-walkthrough.Rproj file in the folder you just unzipped
* Now we just need to install the needed packages and you can copy the below to the R console.
  * If (on Windows) you are asked "Do you want to install from source the packages which need compilation", then say "No".

```{r, eval = FALSE, warning=FALSE}
Sys.setenv(R_REMOTES_NO_ERRORS_FROM_WARNINGS="TRUE")

install.packages("devtools", type = "binary")
install.packages("BiocManager")
pkgbuild::find_rtools()

library(BiocManager)


packs <- c("purrr", "dplyr")
install(packs, ask=FALSE, INSTALL_opts=c("--no-multiarch"))

packs <- c("xcms", "CAMERA", "readxl")
install(packs, ask=FALSE, INSTALL_opts=c("--no-multiarch"))

packs <- c("lgatto/ProtGenerics", "sneumann/mzR", "lgatto/MSnbase", 
           "sneumann/xcms", "sneumann/CAMERA", "stanstrup/commonMZ"
          )
install(packs, ask=FALSE, INSTALL_opts=c("--no-multiarch"))

```

<details>
  <summary><em>Click for more explanations of each line of code</em></summary>
  
  The first line sets a special (environmental) variable. By default the installation of packages will be stopped if there are any warnings. But there are common warnings about mis-matching versions between your own installation of R and the version used to build the packages. These warnings are almost always of no consequence so we don't want to fail because of them.
  
The `install.packages` function tells R to install certain package.<br>
For `devtools` we also tell it to use the binary version of the package (this is on Windows) even if there exist source code for a never version. We do this because `devtools` depend on many packages and sometimes it is very difficult to get them all to match otherwise.

The `pkgbuild::find_rtools` function makes R figure out where the `Rtools` you installed is located. This is needed to build some packages from source code.

`library(BiocManager)` says to load the `BiocManager` package that we will use to install other packages. `install` is the function from `BiocManager`. `ask=FALSE` says to not ask if we want to update old packages while we install a new package; just do the update of all packages.

The `INSTALL_opts=c("--no-multiarch")` options avoid R insisting on building the packages also for 32 bit R even if you are using 64 bit R (you should!).

Packages named like `"sneumann/xcms"` means that the package's source code should be taken from GitHub and compiled. We do this for some packages to use the very latest code available.
</details> 

### Troubleshooting

* If R complain that it cannot find some files like `gcc`, `make` or `cygwin1.dll` (the error is even wrong. It is `msys-2.0.dll` that it needs.) you need to adjust your path. In Windows:
  * Open the windows file browser
  * right-click "This PC" --> "Properties"
  * On the left click "Advanced system settings"
  * Click "Environmental Variables..."
  * Find "Path" and "C:\rtools40\mingw64\bin\" and "C:\rtools40\usr\bin" (no quotes) at the top of the list. Do for both "User variables" and "System variables".


## Browsing {LC, GC}-MS data

* [MZmine](http://mzmine.github.io/download.html) can read all open formats and have a good GUI for looking at your data.
* Your teacher might provide you with MassLynx, which is the software that Waters provide for acquiring and looking at their data (in the proprietary format they record data in).


## Converting data

You should install [ProteoWizard](http://proteowizard.sourceforge.net/download.html) so that you can convert raw data to an open format like mzML that can be read by xcms and MZmine.
