<!---
(--------------------------------------------)

(PLEASE EDIT THE RMD FILE, NOT THE MD FILE!)

(--------------------------------------------)
-->

# Coffee study walkthrough

1.  [Installing
    Software](https://nexs-metabolomics.gitlab.io/INM/coffee-walkthrough/01_installing_software.html)
2.  [Downloading
    Data](https://nexs-metabolomics.gitlab.io/INM/coffee-walkthrough/02_downloading_data.html)
3.  [Converting
    Files](https://nexs-metabolomics.gitlab.io/INM/coffee-walkthrough/03_converting_files.html)
4.  [Creating
    Metadata](https://nexs-metabolomics.gitlab.io/INM/coffee-walkthrough/04_creating_metadata.html)
5.  [XCMS](https://nexs-metabolomics.gitlab.io/INM/coffee-walkthrough/05_XCMS.html)
6.  [CAMERA](https://nexs-metabolomics.gitlab.io/INM/coffee-walkthrough/06_CAMERA.html)
7.  [Troubleshooting](https://nexs-metabolomics.gitlab.io/INM/coffee-walkthrough/07_troubleshooting.html)
8.  [Univariate
    Statistics](https://nexs-metabolomics.gitlab.io/INM/coffee-walkthrough/08_univariate_statistics.html)
