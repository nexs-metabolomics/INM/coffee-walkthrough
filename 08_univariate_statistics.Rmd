---
output: 
  html_document: 
    df_print: kable
    highlight: espresso
    theme: lumen
    css: styles.css
editor_options: 
  chunk_output_type: console
title: Univariate statistical analysis
---

```{r setup, include=FALSE}
library(DT)
source("scripts/FUNS.R")
```

## Nesting

Before we are ready to run the univariate statistics we would like to group our data. We group such that each group contains the data from one feature. We then then run statistics on each group separately.

The way we do the grouping is by **nesting** the data. We tell the `nest` function which functions should be nested. That would be all columns related to the samples as opposed to those related the the features.
Probably an example is better...

```{r nest, message=FALSE}
library(tidyr)
library(dplyr)

peaklist_long_nest_samples <- peaklist_long %>% 
                                filter(sample_type=="sample") %>% 
                                mutate_at(vars(subject, time), as.factor) %>% 
                                nest(data = c(peakidx, 
                                              mz, mzmin, mzmax, rt, rtmin, rtmax, 
                                              into, intb, maxo, 
                                              sn, egauss, mu, sigma, h, f, dppm, scale, scpos, scmin, scmax, lmin, lmax, 
                                              fromFile, is_filled, filename, filepath, 
                                              desc, msfile, inletfile, bottle, injection_vol, sample_type, MSE, 
                                              inj_order, subject, time, gender, drink
                                              )
                                     )


```



```{r nest2, message=FALSE, eval=-1, echo=-c(3:7)}
peaklist_long_nest_samples

peaklist_long_nest_samples %>% 
      slice(1:10) %>% 
      datatable_nice() %>% 
      formatRound(columns=c("f_mzmed", "f_mzmin", "f_mzmax"), digits=4) %>% 
      formatRound(columns=c("f_rtmed", "f_rtmin", "f_rtmax"), digits=2)
```
<i>subset of the table only</i>

<br><br>
See how we now have a column named data? This column is in reality a list of data.frames/tibbles (one for each row/feature) with the data for each feature that is described in the columns you see.



The data from the first feature can be access like this:

```{r nest3, message=FALSE, eval=-1, echo=-c(3:7)}
peaklist_long_nest_samples$data[[1]]

peaklist_long_nest_samples$data[[1]] %>% 
      mutate(is_filled = toupper(as.character(is_filled))) %>% 
      datatable_nice() %>% 
      formatRound(columns=c("mz", "mzmin", "mzmax", "egauss", "h"), digits=4) %>% 
      formatRound(columns=c("rt", "rtmin", "rtmax", "into", "intb", "maxo", "mu", "sigma"), digits=2)
```

It is such subsets of the data we will run the univariate model on. We can see that there are 36 rows representing the samples for the 12 subjects × 3 time points.


## Linear model

Before we are ready to run the statistics we will make some special version of the statistical functions we need.
The problem we need to solve is that for some of the features we do not have enough non-zero values to make a model. The model will therefore fail and R will stop.
What we can do is make a function that in such cases just outputs `NA` without failing. That is what `possibly` (from the `purrr` package) does. So we make special versions of the `lm` and `anova` functions and use those instead.
For the `tidy` function we need the problem is a bit more complicated because it doesn't fail if the input is `NA` but we need the output to be similar to the output you get when it works so we make a function that handles the case when `anova` failed explicitly.


```{r stat0, message=FALSE}
library(broom)
library(purrr)
library(fdrtool)

lm_p <- possibly(lm, NA)
anova_p <- possibly(anova, NA)

tidy_p <- possibly(tidy, tibble(p.value = NA))
tidy_p2 <- function(x){if(class(x)[1]!="anova"){tibble(p.value = NA)}else{tidy_p(x)} }
```



To run the statistics on all features separately we use the `map` function (from the `purrr` package) to do it in a very "clean" way. `map` is a function that runs through all these nested data.frames and runs some function on each of them. I would normally make one piped flow for all the next steps for for illustrative purposeses we do each step separately here so we can take a look at the output as we move along.

The first step is fitting the model. Or rather we will actually fit two models.
We do that with the `lm` (linear models) function and we define the first model as `into ~ drink*time` which means that the area (`into` column) is modeled by the main effects and interaction of drink and time. Then we also make a model without the `time` factor. The strategy is to compare those two models. For features where the fit is significantly different we have features that were affected by what people drank.

We could have made more elaborate models that included gender and person specific effects but we keep it simple for now. Gender and person should also be modeled as random effects which `lm` cannot do so we would have needed the `lmer` function from the `lme4` package.

So here are the two models:

```{r stat1, message=FALSE}
stat1 <- peaklist_long_nest_samples %>% 
          mutate(model1 = map(data, ~lm_p(into ~ drink*time, data = .x))) %>% 
          mutate(model2 = map(data, ~lm_p(into ~ time,       data = .x)))
```


<details>
  <summary><em>Click for more explanations of each line of code</em></summary>
  We take the nested dataset and then mutate like we have done before to make a new column with the models.
  The way `map` works is that when you use it inside a `mutate` the first argument is the column it should go through. The second argument is a so called anonymous function that says what to do with the input. `.x` refers to the input to `map` (one at a time).
</details>



## ANOVA

<br><br>
The next step is running the ANOVA comparison with `anova`. `map2` is a variant of `map` that takes two inputs and a function that works on those two arguments.

```{r stat2, message=FALSE}
stat2 <- stat1 %>% 
          mutate(anova = map2(model1, model2, anova_p))
```


We can take a look at one of the ANOVA results and we see the p-value calculated.
```{r stat2-2, message=FALSE, echo=TRUE, eval=FALSE}
stat2$anova[[98]]
```

```{r stat2-2-DT, message=FALSE, echo=FALSE, eval=TRUE}
stat2$anova[[98]] %>% datatable(rownames=FALSE, options = list(bPaginate = FALSE, sDom  = '<"top">'))
```

<br><br>
The `anova` result object can be converted to a normal table with `as.data.frame()` but we are going to use the `tidy` function (from the `broom` package) to create the table with the p-values (and other stats) because that standardized the output between different stats functions.

```{r stat3, message=FALSE, warning=FALSE}
stat3 <- stat2 %>% 
          mutate(stats = map(anova, tidy_p2))
```

It looks like this:
```{r stat3-2, message=FALSE, echo=TRUE, eval=FALSE}
stat3$stats[[98]]
```

```{r stat3-2-DT, message=FALSE, echo=FALSE, eval=TRUE}
stat3$stats[[98]] %>% datatable(rownames=FALSE, options = list(bPaginate = FALSE, sDom  = '<"top">'))
```

You can see in this case only the column names are different.

<br><br>
We now just have a bit of house keeping to do.

```{r stat-final, message=FALSE}
stat <- stat3 %>%
          select(-data, -model1, -model2, -anova) %>% 
          mutate(stats = map(stats, ~ ..1 %>% slice(2))) %>% 
          unnest(stats) %>% 
          select(-res.df, -rss, -df, -sumsq, -statistic)

rm(stat1, stat2, stat3)
```

<details>
  <summary><em>Click for more explanations of each line of code</em></summary>
  We use `select` to remove columns we don't need to keep. These are all nested columns and we can only have one nested column when we unnest.
  We then use `map` and `slice` again to go into through those tables you saw just above to take only the second row since that is what has the p-value.
  We then `unnest` so that we no longer have the nested structure.
  Then we remove columns from the stats that we have no use for.
</details>


## FDR

The final calculation we need to do is get FDR q-values.
          
```{r fdr, message=FALSE}
stat <- stat %>%
          mutate(q.value = fdrtool(p.value, statistic = "pvalue", plot = FALSE)$qval)
```


<details>
  <summary><em>Click for more explanations of each line of code</em></summary>
  This time no nesting! The function `fdrtool` takes the whole selection of p-values and estimates the FDR.
  `fdrtool` outputs two values and we use `$qval` to grab the q-value.
</details>



## Checking the results

Now lets filter by the q-value and see the significant features.

```{r stat-results, message=FALSE, echo=TRUE, eval=FALSE}
stat %>% 
  filter(q.value<0.05) %>% 
  mutate_at(vars(f_rtmed, f_rtmin, f_rtmax), ~.x/60)
```

```{r stat-results-DT, message=FALSE, echo=FALSE, eval=TRUE}
stat %>% 
  filter(q.value<0.05) %>% 
  mutate_at(vars(f_rtmed, f_rtmin, f_rtmax), ~.x/60) %>% 
  datatable_nice() %>% 
  formatSignif(columns=c("p.value", "q.value"), digits=2) %>% 
  formatRound(columns=c("f_mzmed", "f_mzmin", "f_mzmax"), digits=4) %>% 
  formatRound(columns=c("f_rtmed", "f_rtmin", "f_rtmax"), digits=2)
```

<br><br>

We can do slightly better though. In the above there could be cases where not all features from one pcgroup was significant. But for the purpose of identification we probably would like all features in those pcgroups where one feature is significant.


```{r stat-results-fullpcgroup, message=FALSE, eval=FALSE, echo=TRUE}
stat %>% 
  filter(q.value<0.05) %>% 
  distinct(mode, pcgroup) %>% 
  left_join(stat, by = c("mode", "pcgroup")) %>% 
  distinct(mode, feature_id, f_mzmed, f_rtmed, isotopes, adduct, pcgroup, p.value, q.value) %>% 
  mutate(f_rtmed = f_rtmed/60) %>% 
  mutate_at(vars(p.value, q.value), ~formatC(.x, format = "e", digits = 1)) %>% 
  arrange(mode, pcgroup, f_mzmed)
```

```{r stat-results-fullpcgroup-DT, message=FALSE, eval=TRUE, echo=FALSE}
stat %>% 
  filter(q.value<0.05) %>% 
  distinct(mode, pcgroup) %>% 
  left_join(stat, by = c("mode", "pcgroup")) %>% 
  select(mode, feature_id, f_mzmed, f_rtmed, isotopes, adduct, pcgroup, p.value, q.value) %>% 
  mutate(f_rtmed = f_rtmed/60) %>% 
  mutate_at(vars(p.value, q.value), ~formatC(.x, format = "e", digits = 1)) %>% 
  arrange(mode, pcgroup, f_mzmed) %>% 
  datatable_nice() %>% 
  formatRound(columns=c("f_mzmed"), digits=4) %>% 
  formatRound(columns=c("f_rtmed"), digits=2)
```

<details>
  <summary><em>Click for more explanations of each line of code</em></summary>
  First we take only features/rows where the q-value is less than 0.05.
  Then we take all distinct combinations of the mode and the pcgroup. That gives is a table with just the pcgroups we are interested in.
  Now we take the `stat`s table again and join it to that table with the selected pcgroups. That gives is all features that match.
  we `select` the columns we want, make RT in minutes, set p-values and q-values to be scientific notation and order (`arrange`) the table sensibly.
  
</details>
